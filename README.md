# gofast-technical-weblate

## Getting started

This repo is solely intended to store .po files for webex integration. It must not contain anything else.

## Convention

Every strings in .po file must have context gofast_technical:CONTEXT. The source strings must be technical strings following this format:
typocontext:featurecontext:subfeaturecontext:item.
For example: tooltip:kanban:column:card-hover

## Add your files

- For each context, a subfolder named as the context must be created. Each folder must then match a weblate component.
- Files must have the following name : LANGCODE_gofast_technical_CONTEXT.po.
- Langcode used for english must be "gfen" instead of "en".